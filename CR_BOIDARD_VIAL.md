Compte rendu Move 
Kylian BOIDARD ; Mathieu VIAL   

# PART 1 - Tools and techno introduction

## 1.1 URDF comes to life !
The launched nodes are : 
    joint_state_publisher (joint_state_publisher/joint_state_publisher)
    robot_state_publisher (robot_state_publisher/state_publisher)
    rviz (rviz/rviz)

xacro is a macro language for XML, it allows to create and use variables in the URDF files. 
In the file scara_cpe.xacro, this feature is used to create some xacro properties that can be reused in the document to setup parameters. 

There is several joints in that xacro file : base_fixed, camera_fixed, shoulder_1_joint, shoulder_2_joint, end_joint


As there is 0.048m in translation betweeen base link and link 1, 0.08m between link1 and link2, and 0.047m between link2 and end_joint, the total Y-Distance when the arm is straight is 0.175m. 
We can also use : 

    triton_01@triton-01:~/ws_project_move$ rosrun tf tf_echo base_link end_link
    At time 833.788
    - Translation: [-0.000, 0.175, -0.025]
    - Rotation: in Quaternion [0.000, 0.000, 0.000, 1.000]
                in RPY (radian) [0.000, -0.000, 0.000]
                in RPY (degree) [0.000, -0.000, 0.002]


## 1.2 GAZEBO simulation and Rviz vizualisation

**What is Gazebo ?**
Gazebo is a 3D robot simulator. Its objective is to simulate a robot, giving you a close substitute to how your robot would behave in a real-world physical environment. It can compute the impact of forces (such as gravity).

**What is Rviz ?** 
Rviz (short for “ROS visualization”) is a 3D visualization software tool for robots, sensors, and algorithms. It enables you to see the robot’s perception of its world (real or simulated).
The purpose of rviz is to enable you to visualize the state of a robot. It uses sensor data to try to create an accurate depiction of what is going on in the robot’s environment.

**In what are they completly different tools ?**
The difference between the two can be summed up in the following excerpt from Morgan Quigley (one of the original developers of ROS) in his book Programming Robots with ROS:
“rviz shows you what the robot thinks is happening, while Gazebo shows you what is really happening.”
Gazebo is the actual real world physics simulator that you will set up a world and simulate your robot moving around. Rviz is the visualization software, that will allow you to view that gazebo data (if you are simulating) or real world data (if you are not using gazebo, but a real robot). 


**Coordinates of the cylinders :**
model name='A_point' -->  pose -0.073 0.08 0.0005 0 0 0 --> Color RED

model name='B_point' --> pose 0.035 0.14 0.0005 0 0 0 --> Color Green

model name='C_point' --> pose 0.044 0.08 0.0005 0 0 0 --> Color Blue 

model name='D_point' --> pose -0.035 0.12 0.0005 0 0 0 --> Color Yellow

model name='unit_cylinder_1' --> pose 0.067 0.11 0.5 0 0 0 --> Color Purple

## 1.3 ROS Actions and controllers
By listing topics, we can identify the ROS Action: follow_joint_trajectory

    triton_01@triton-01:~/ws_project_move$ rostopic list 

    /scara_cpe/scara_cpe_controller/follow_joint_trajectory/cancel
    /scara_cpe/scara_cpe_controller/follow_joint_trajectory/feedback
    /scara_cpe/scara_cpe_controller/follow_joint_trajectory/goal
    /scara_cpe/scara_cpe_controller/follow_joint_trajectory/result
    /scara_cpe/scara_cpe_controller/follow_joint_trajectory/status



# PART 2 - Motion Planning with MoveIt!
## 2.1 Introduction to MoveIt!

**What are the limitations of the default solver ?**
Le default solver n'est pas très permissif car il faut impérativement un système avec 6 DoF pour qu'il fonctionne. Cela s'explique car on envoie une Pose à 6 coordonnées, il faut donc 6 DoF pour atteindre la position et l'orientation désirée.

# PART 3 - Link MoveIt! to the robot (real)


# PART 4 - Link MoveIt! to the robot (gazebo)
## 4.1 Link Gazebo controller and joint_state feedback

## 4.3 Add a sensor (Kinect from Gazebo) to MoveIt!

**Comment what you observe. Is the robot arm considered as an obstacle ? Can we see the purple cylinder obstacle ? What about the box ? With a more 
appropriate environment setting, what is the necessary strategy to avoid obstacles ?**
Les différents objets vus par la kinect sont détectés, et le bras du robot est considéré commme un obstacle, car il apparait en rouge dans le planning scene. 
Le cylindre violet n'est pas considéré comme obstacle. 
Une stratégie est de considérer ces objets comme obstacle dans Rviz. 


# PART 5 - Planning Scene with collision avoidance


**Maybe have you seen the electro-magnet touching the cylinder obstacle. In Rviz, click on this check box MotionPlanning->Scene Robot->Show Robot Collision and explain why a slight collision might appear.
In scara_cpe_moveit, modify values in kinematics.yaml, test, and explain the pros and cons to consider in the fine tuning.**

Il peut y avoir de légères collisions car les zones sont définies comme des rectangles et ne sont pas représentative de la réalité. Il peut donc y avoir un contact réel non visible dans Rviz.
En modifiant kinematics_solver_search_resolution on peut obtenir une meilleure résolution au détrimennt du temps de calcul qui sera rallongé.


