#!/usr/bin/env python
import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg
from math import pi
from std_msgs.msg import String
from moveit_commander.conversions import pose_to_list
import time

class moveit_planning_target:
    def __init__(self):
        moveit_commander.roscpp_initialize(sys.argv)
        rospy.init_node('move_scara', anonymous=True)
        self.group_name = "scara_cpe_group"
        self.robot = moveit_commander.RobotCommander()
        self.move_group = moveit_commander.MoveGroupCommander(self.group_name)
        self.box_name = ''

        self.scene = moveit_commander.PlanningSceneInterface()
        self.move_group.set_goal_position_tolerance(0.010)
        self.move_group.set_goal_orientation_tolerance(2*pi)

    def init_object(self):
        self.cylinder_pose = geometry_msgs.msg.PoseStamped()
        self.cylinder_pose.header.frame_id = "world"
        self.cylinder_pose.pose.orientation.w = 1
        self.cylinder_pose.pose.position.z = 0.5 # slightly above the end effector
        self.cylinder_pose.pose.position.x = 0.067
        self.cylinder_pose.pose.position.y = 0.11
        self.cylinder_name = "cylinder"

        self.coin_A_pose = geometry_msgs.msg.PoseStamped()
        self.coin_A_pose.header.frame_id = "world"
        self.coin_A_pose.pose.orientation.w = 1
        self.coin_A_pose.pose.position.z = 0.0005 # slightly above the end effector
        self.coin_A_pose.pose.position.x = -0.073
        self.coin_A_pose.pose.position.y = 0.08
        self.coin_A_name = "coin_A"

        self.coin_B_pose = geometry_msgs.msg.PoseStamped()
        self.coin_B_pose.header.frame_id = "world"
        self.coin_B_pose.pose.orientation.w = 1
        self.coin_B_pose.pose.position.z = 0.0005 # slightly above the end effector
        self.coin_B_pose.pose.position.x = 0.035
        self.coin_B_pose.pose.position.y = 0.14
        self.coin_B_name = "coin_B"

        self.coin_C_pose = geometry_msgs.msg.PoseStamped()
        self.coin_C_pose.header.frame_id = "world"
        self.coin_C_pose.pose.orientation.w = 1
        self.coin_C_pose.pose.position.z = 0.0005 # slightly above the end effector
        self.coin_C_pose.pose.position.x = 0.044
        self.coin_C_pose.pose.position.y = 0.08
        self.coin_C_name = "coin_C"
        
        self.coin_D_pose = geometry_msgs.msg.PoseStamped()
        self.coin_D_pose.header.frame_id = "world"
        self.coin_D_pose.pose.orientation.w = 1
        self.coin_D_pose.pose.position.z = 0.0005 # slightly above the end effector
        self.coin_D_pose.pose.position.x = -0.035
        self.coin_D_pose.pose.position.y = 0.12
        self.coin_D_name = "coin_D"
        self.eef_link=self.move_group.get_end_effector_link()
        
        time.sleep(2)

        self.scene.add_cylinder(self.cylinder_name,self.cylinder_pose,0.08,0.015)
        self.wait_for_state_update()
        self.scene.add_cylinder(self.coin_A_name,self.coin_A_pose,0.001,0.015)
        self.wait_for_state_update()
        self.scene.add_cylinder(self.coin_B_name,self.coin_B_pose,0.001,0.015)
        self.wait_for_state_update()
        self.scene.add_cylinder(self.coin_C_name,self.coin_C_pose,0.001,0.015)
        self.wait_for_state_update()
        self.scene.add_cylinder(self.coin_D_name,self.coin_D_pose,0.001,0.015)
        self.wait_for_state_update()
    
    def move(self):
        
        xyz=[self.coin_C_pose.pose.position.x,self.coin_C_pose.pose.position.y,0.01]
        self.move_group.set_position_target(xyz)
        self.move_group.go(wait=True)
        time.sleep(1)
        self.attach_coins("coin_C")
        time.sleep(1)
        self.move_group.set_named_target('straight')
        self.move_group.go(wait=True)
        time.sleep(1)
        self.detach_coins("coin_C")
        time.sleep(1)
        xyz=[self.coin_B_pose.pose.position.x,self.coin_B_pose.pose.position.y,0.01]
        self.move_group.set_position_target(xyz)
        self.move_group.go(wait=True)
        time.sleep(1)
        self.attach_coins("coin_B")
        time.sleep(1)
        xyz=[self.coin_C_pose.pose.position.x,self.coin_C_pose.pose.position.y,0.01]
        self.move_group.set_position_target(xyz)
        self.move_group.go(wait=True)
        time.sleep(1)
        self.detach_coins("coin_B")
        time.sleep(1)
        xyz=[self.coin_D_pose.pose.position.x,self.coin_D_pose.pose.position.y,0.01]
        self.move_group.set_position_target(xyz)
        self.move_group.go(wait=True)
        time.sleep(1)
        self.attach_coins("coin_D")
        time.sleep(1)
        xyz=[self.coin_B_pose.pose.position.x,self.coin_B_pose.pose.position.y,0.01]
        self.move_group.set_position_target(xyz)
        self.move_group.go(wait=True)
        time.sleep(1)
        self.detach_coins("coin_D")
        time.sleep(1)
        xyz=[self.coin_A_pose.pose.position.x,self.coin_A_pose.pose.position.y,0.01]
        self.move_group.set_position_target(xyz)
        self.move_group.go(wait=True)
        time.sleep(1)
        self.attach_coins("coin_A")
        time.sleep(1)
        xyz=[self.coin_D_pose.pose.position.x,self.coin_D_pose.pose.position.y,0.01]
        self.move_group.set_position_target(xyz)
        self.move_group.go(wait=True)
        time.sleep(1)
        self.detach_coins("coin_A")
        time.sleep(1)
        self.scene.remove_world_object("cylinder")
    
        self.scene.remove_world_object("coin_A")
        self.scene.remove_world_object("coin_B")
        self.scene.remove_world_object("coin_C")
        self.scene.remove_world_object("coin_D")
            

    def attach_coins(self, coin_name):
        grasping_group = 'scara_cpe_group'
        touch_links = self.robot.get_link_names(group=grasping_group)
        self.scene.attach_box(self.eef_link,coin_name,touch_links=touch_links)
        return self.wait_for_state_update(box_is_attached=True, box_is_known=False)
    
    def detach_coins(self, coin_name):
        self.scene.remove_attached_object(self.eef_link, name=coin_name)
        return self.wait_for_state_update(box_is_known=True, box_is_attached=False)
    
    def wait_for_state_update(self, box_is_known=False, box_is_attached=False, timeout=4):
    # Copy class variables to local variables to make the web tutorials more clear.
    # In practice, you should use the class variables directly unless you have a good
    # reason not to.
        box_name = self.box_name
        scene = self.scene

        ## BEGIN_SUB_TUTORIAL wait_for_scene_update
        ##
        ## Ensuring Collision Updates Are Received
        ## ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
        ## If the Python node dies before publishing a collision object update message, the message
        ## could get lost and the box will not appear. To ensure that the updates are
        ## made, we wait until we see the changes reflected in the
        ## ``get_attached_objects()`` and ``get_known_object_names()`` lists.
        ## For the purpose of this tutorial, we call this function after adding,
        ## removing, attaching or detaching an object in the planning scene. We then wait
        ## until the updates have been made or ``timeout`` seconds have passed
        start = rospy.get_time()
        seconds = rospy.get_time()
        while (seconds - start < timeout) and not rospy.is_shutdown():
            # Test if the box is in attached objects
            attached_objects = scene.get_attached_objects([box_name])
            is_attached = len(attached_objects.keys()) > 0

            # Test if the box is in the scene.
            # Note that attaching the box will remove it from known_objects
            is_known = box_name in scene.get_known_object_names()

            # Test if we are in the expected state
            if (box_is_attached == is_attached) and (box_is_known == is_known):
                return True

            # Sleep so that we give other threads time on the processor
            rospy.sleep(0.1)
            seconds = rospy.get_time()

        # If we exited the while loop without returning then we timed out
        return False

            

if __name__ == '__main__':
      move_robot=moveit_planning_target()
      while not rospy.is_shutdown():
        move_robot.init_object()
        move_robot.move()   