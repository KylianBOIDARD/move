#!/usr/bin/env python

import sys
import copy
import rospy
import rosnode
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg
import time
from math import pi
from std_msgs.msg import Bool
from moveit_commander.conversions import pose_to_list


class MoveCoins :

    def __init__(self) :
        
        moveit_commander.roscpp_initialize(sys.argv)
        rospy.init_node('move_coins', anonymous=True)
        self.move_group = moveit_commander.MoveGroupCommander("scara_cpe_group")

        self.grip_pub = rospy.Publisher("/grip", Bool, queue_size=10)

        self.pose_goal_a = geometry_msgs.msg.Pose() ; self.pose_goal_a.orientation.w = 1.0 ; self.pose_goal_a.position.x = -0.073 ; self.pose_goal_a.position.y = 0.08 ; self.pose_goal_a.position.z = 0.01
        self.pose_goal_b = geometry_msgs.msg.Pose() ; self.pose_goal_b.orientation.w = 1.0 ; self.pose_goal_b.position.x = 0.035 ; self.pose_goal_b.position.y = 0.14 ; self.pose_goal_b.position.z = 0.01


    def move_gripper(self, pose):

        self.move_group.set_joint_value_target(pose, True)
        self.move_group.go(wait=True) 

    def catch_coin(self):
        true = Bool()
        true.data = True
        self.grip_pub.publish(true)
    
    def release_coin(self):
        false = Bool()
        false.data = False
        self.grip_pub.publish(false)
    
    def process(self):

        self.move_gripper(self.pose_goal_a)
        time.sleep(1)
        self.catch_coin()
        time.sleep(1)
        self.move_gripper(self.pose_goal_b)
        time.sleep(1)
        self.release_coin()


if __name__ == '__main__':
    movecoins = MoveCoins()
    movecoins.process()



