#!/usr/bin/env python

import sys
import copy
import rospy
import rosnode
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg
from math import pi
from std_msgs.msg import String
from moveit_commander.conversions import pose_to_list




class moveit_test_poses():

    def __init__(self) :
        moveit_commander.roscpp_initialize(sys.argv)
        rospy.init_node('moveit_test_named_poses', anonymous=True)
        self.move_group = moveit_commander.MoveGroupCommander("scara_cpe_group")
        self.pose_goal_a = geometry_msgs.msg.Pose() ; self.pose_goal_a.orientation.w = 1.0 ; self.pose_goal_a.position.x = -0.073 ; self.pose_goal_a.position.y = 0.08 ; self.pose_goal_a.position.z = 0.01
        self.pose_goal_b = geometry_msgs.msg.Pose() ; self.pose_goal_b.orientation.w = 1.0 ; self.pose_goal_b.position.x = 0.035 ; self.pose_goal_b.position.y = 0.14 ; self.pose_goal_b.position.z = 0.01
        self.pose_goal_c = geometry_msgs.msg.Pose() ; self.pose_goal_c.orientation.w = 1.0 ; self.pose_goal_c.position.x = 0.4 ; self.pose_goal_c.position.y = 0.1 ; self.pose_goal_c.position.z = 0.01
        self.pose_goal_d = geometry_msgs.msg.Pose() ; self.pose_goal_d.orientation.w = 1.0 ; self.pose_goal_d.position.x = 0.4 ; self.pose_goal_d.position.y = 0.1 ; self.pose_goal_d.position.z = 0.01
    
        

    
    # def update_poses(self):
    #     """ Using set_pose_target"""
    #     self.move_group.set_goal_orientation_tolerance(2*pi)
    #     while not rospy.is_shutdown() :
    #         self.move_group.set_pose_target(self.pose_goal_a)
    #         self.move_group.go(wait=True)  
    #         self.move_group.set_pose_target(self.pose_goal_b)
    #         self.move_group.go(wait=True)  
    
    # def update_poses(self):
    #     """ Using set_position_target"""

    #     xyz_a = [self.pose_goal_a.position.x, self.pose_goal_a.position.y, self.pose_goal_a.position.z]
    #     xyz_b = [self.pose_goal_b.position.x, self.pose_goal_b.position.y, self.pose_goal_b.position.z]

    #     while not rospy.is_shutdown() :
    #         self.move_group.set_position_target(xyz_a)
    #         self.move_group.go(wait=True)  
    #         self.move_group.set_position_target(xyz_b)
    #         self.move_group.go(wait=True)  
    
    def update_poses(self):
        """ Using set_joint_value_target"""
        while not rospy.is_shutdown() :
            self.move_group.set_joint_value_target(self.pose_goal_a, True)
            self.move_group.go(wait=True)  
            self.move_group.set_joint_value_target(self.pose_goal_b, True)
            self.move_group.go(wait=True)  


if __name__ == '__main__':
    x = moveit_test_poses()
    x.update_poses()
    
