#!/usr/bin/env python

import sys
import copy
import rospy
import rosnode
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg
from math import pi
from std_msgs.msg import String
from moveit_commander.conversions import pose_to_list



class moveit_test_path():

    def __init__(self) :
        moveit_commander.roscpp_initialize(sys.argv)
        rospy.init_node('moveit_test_named_path', anonymous=True)
        self.move_group = moveit_commander.MoveGroupCommander("scara_cpe_group")

        self.pose_goal_a = geometry_msgs.msg.Pose() ; self.pose_goal_a.orientation.w = 1.0 ; self.pose_goal_a.position.x = -0.073 ; self.pose_goal_a.position.y = 0.08 ; self.pose_goal_a.position.z = 0.01
        self.pose_goal_b = geometry_msgs.msg.Pose() ; self.pose_goal_b.orientation.w = 1.0 ; self.pose_goal_b.position.x = 0.035 ; self.pose_goal_b.position.y = 0.14 ; self.pose_goal_b.position.z = 0.01



    def update_path(self):
        """ Using compute_cartesian_path"""

        self.move_group.set_named_target("left")
        self.move_group.go(wait=True)
        waypoints = []
        waypoints.append(copy.deepcopy(self.pose_goal_a))
        waypoints.append(copy.deepcopy(self.pose_goal_b))
        waypoints2 = []
        waypoints2.append(copy.deepcopy(self.pose_goal_b))
        waypoints2.append(copy.deepcopy(self.pose_goal_a))


        while not rospy.is_shutdown() :
            plan_a, fraction_a = self.move_group.compute_cartesian_path(waypoints, 0.01, 0.0)
            self.move_group.execute(plan_a, wait=True)
            plan_b, fraction_b = self.move_group.compute_cartesian_path(waypoints2, 0.01, 0.0)
            self.move_group.execute(plan_b, wait=True)
            


        



if __name__ == '__main__':
    x = moveit_test_path()
    x.update_path()




