#!/usr/bin/env python

import sys
import copy
import rospy
import rosnode
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg
import time
from math import pi
from std_msgs.msg import String
from moveit_commander.conversions import pose_to_list



class moveit_test_named_target():

    def __init__(self) :

        moveit_commander.roscpp_initialize(sys.argv)
        rospy.init_node('moveit_test_named_target', anonymous=True)
        self.move_group = moveit_commander.MoveGroupCommander("scara_cpe_group")
        
    def update_names(self):
        names = self.move_group.get_named_targets()
        #print(names)
        while not rospy.is_shutdown() :
            for name in names:
                self.move_group.set_named_target(name)
                self.move_group.go(wait=True)  


if __name__ == '__main__':
    x = moveit_test_named_target()
    x.update_names()
    
