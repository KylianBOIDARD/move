#!/usr/bin/env python
import roslib
roslib.load_manifest('scara_cpe_kinematics')

import rospy
import actionlib
import control_msgs.msg
import math

from trajectory_msgs.msg import JointTrajectoryPoint
from control_msgs.msg import JointTrajectoryAction, JointTrajectoryGoal, FollowJointTrajectoryAction, FollowJointTrajectoryGoal

class Joint():
    def __init__(self):
        self.jta = actionlib.SimpleActionClient('/scara_cpe/scara_cpe_controller/follow_joint_trajectory', FollowJointTrajectoryAction)
        rospy.loginfo('Waiting for joint trajectory action')
        self.jta.wait_for_server()
        rospy.loginfo('Found joint trajectory action!')

    def move_joint(self, angles):
        goal = FollowJointTrajectoryGoal()

        goal.trajectory.joint_names = ['shoulder_1_joint', 'shoulder_2_joint']

        point = JointTrajectoryPoint()

        point.positions = angles
        point.time_from_start = rospy.Duration(3)

        goal.trajectory.points.append(point)

        self.jta.send_goal_and_wait(goal)
    
    def calculate_angles(self, x, y):
        l1 = 0.08 
        l2 = 0.047
        y += 0.048
        print(x,y)

        # if( math.sqrt(x**2+y**2) > (l1+l2) or (math.sqrt(x**2+y**2)<(l1-l2)) ):       #verification des coordonnees
        #     print("Zone inaccessible") 
        #     return

        costheta2 = (x*x+y*y-l1*l1-l2*l2)/(2*l1*l2) 
        #costheta2 = (l1*l1+l2*l2-x*x-y*y)/(2*l1*l2) 
        print(costheta2)

        #if( abs( costheta2 ) ) :
        theta2 = math.acos( costheta2 ) ;     #calcul de theta2
            # COUDE BAS
        theta1 = math.atan2(l2*math.cos(theta2)*y-l2*math.sin(theta2)*x+l1*y,l2*math.sin(theta2)*y+l2*math.cos(theta2)*x+l1*x) 

        #if (3.14159>= theta1 >=0 and -2.96706<=theta2<=2.96706):
        return [theta1,theta2]
            
            #COUDE HAUT 
        theta1=math.atan2(l2*math.cos(-theta2)*y-l2*math.sin(-theta2)*x+l1*y,l2*math.sin(-theta2)*y+l2*math.cos(-theta2)*x+l1*x)
        return [theta1,-theta2]


def main():
            arm = Joint()
            angles = arm.calculate_angles(0.035, 0.14)
            arm.move_joint(angles)



if __name__ == '__main__':
      rospy.init_node('joint_position_tester')
      main()

